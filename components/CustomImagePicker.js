import React,{useState} from 'react';
import  ImagePicker from 'react-native-image-crop-picker'

import {
    SafeAreaView,
    ScrollView,
    Image,
    StyleSheet,
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    Alert,
  } from 'react-native';
  
  import {Permission,PERMISSION_TYPE} from '../utils/AppPermissions'

const CustomImagePicker = ({

}) => {
    const [imageSource,setImageSource] = useState([])


    checkForCameraPermission = async () => {
        const permission =  await Permission.checkPermission(PERMISSION_TYPE.camera)
   
        console.log("Permission in app :"+permission)
       switch (permission) {
         case true:
             takePhotoFromCamera()
             break
         case false:
             handleDenied()
             break
         default:
                 break
       }
     }

     const takePhotoFromCamera = () => {
        //alert("hello")
        ImagePicker.openCamera({
            width: 1200,
            height: 780,
            cropping: true,
          }).then((image) => {
            console.log(image);
            const imageUri = Platform.OS == 'ios' ? image.sourceURL : image.path
            setImageSource(val => [...val, imageUri])
            console.log("Images: "+ imageSource)
          }).catch(err => {
            Alert.alert(
              'You pressed back button!',
              'Do you want to click an image ?',
              [
                {
                  text:'Cancel',
                  style:'cancel'
                },
                {
                text:'Yes',
                onPress:() => takePhotoFromCamera()
              }
            ],
            {cancelable:false}
            )
          })
      }
    
      const handleDenied = () => {
          Alert.alert(
            'Camera Permissions are required!!',
            'Do you want to accept it ?',
            [
              {
                text:'Cancel',
                style:'cancel'
              },
              {
              text:'Yes',
              onPress:() => checkForCameraPermission()
            }
          ],
          {cancelable:false}
          )
      }
return (
    <ScrollView style ={styles.imagePickerView} contentContainerStyle={{flexDirection:'row',flexGrow:1}} horizontal={true}>

      <View style = {[styles.pickerView,{flexDirection:'row',backgroundColor:'rgba(0,0,0,0.2)'}]}>
        <TouchableOpacity 
          style={{justifyContent:'center',alignItems:'center'}}
          onPress={checkForCameraPermission}
        >
            <Text style={{fontWeight:'bold',fontSize:50,color:'#fff'}}>+</Text>
        </TouchableOpacity>
      </View>

      {imageSource.map(e => 
        <View style = {styles.pickerView} >

          <Image 
            key = {e} source = {{uri:e}}
            style ={{width:120,height:170,borderRadius:10}}
          />
        </View>
        )
      }
    </ScrollView>
)       
    
}

export default CustomImagePicker

const styles = StyleSheet.create({
    imagePickerView: {
        // backgroundColor:'red',
         width:'90%',
         height:200,
         marginTop:10,
       },
       pickerView:{
         alignItems:'center',
         height:170,
         width:120,
         justifyContent:'center',
         marginLeft:15,
         marginTop:15,
         marginLeft:10,
         borderRadius:10
       },

})
