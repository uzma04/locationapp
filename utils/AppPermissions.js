import React from "react";
import {check, request,PERMISSIONS, RESULTS} from 'react-native-permissions';
import {Platform} from 'react-native'

const PLATFORM_CAMERA_PERMISSION = {
  android:PERMISSIONS.ANDROID.CAMERA
}

const PLATFORM_LOCATION_PERMISSION = {
  android:PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION
}

const REQUEST_PERMISSION_TYPE = {
  camera: PLATFORM_CAMERA_PERMISSION,
  location: PLATFORM_LOCATION_PERMISSION
  
}

const PERMISSION_TYPE ={ 
  camera:'camera',
  location:'location'
}

class AppPermissions {

    checkPermission = async (type) :Promise<boolean> => {
      console.log("checkpermission tupe:",+type)
      const permissions = REQUEST_PERMISSION_TYPE[type][Platform.OS]
      console.log("checkPermission permissions :"+permissions)

      if(!permissions) {
        return true
      }
      try{
          const result = await check(permissions)

          if (result === RESULTS.GRANTED) return true
          return this.requestPermission(permissions)//request permission
      }catch(e){
        console.log("checkPermission e1 :"+e)

          return false
      }
    }

    requestPermission = async (permissions) : Promise<boolean> => {
        try{
            const result = await request(permissions)
            return result === RESULTS.GRANTED
        }catch(e) {
          return false
        }
    }
}

const Permission = new AppPermissions()
export {Permission,PERMISSION_TYPE}