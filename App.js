/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React,{useState,useEffect,useCallback} from 'react';
import  ImagePicker from 'react-native-image-crop-picker'
import Geolocation from 'react-native-geolocation-service';
import Geocoder from 'react-native-geocoder';
import {check, request,PERMISSIONS, openSettings} from 'react-native-permissions';

import CustomImagePicker from './components/CustomImagePicker'

import MapView, { PROVIDER_GOOGLE ,Marker} from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps

import {
  SafeAreaView,
  ScrollView,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  Dimensions,
  View,
  TextInput,
  Alert,
  PermissionsAndroid,
} from 'react-native';

const screenWidth = Dimensions.get('window').width

const App = () => {

  const MY_KEY = 'AIzaSyAHXIyDIjIpzKICnUyO_7ZwwuQwrs2ZyjA'
  const [latitude,setLatitude] = useState(0)
  const [longitude,setLongitude] = useState(0)
  const [marker,setMarker] = useState(null)
  const [address,setAddress] = useState('Your address is...')

  var watchID;

  useEffect(() => {
    const requestLocationPermission = async () => {

      // const permission =  await Permission.checkPermission(PERMISSION_TYPE.location)

      // console.log("permission "+permission)
      if (Platform.OS === 'ios') {
        getOneTimeLocation();
        subscribeLocationLocation();
      } else {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
              title: 'Location Access Required',
              message: 'This App needs to Access your location',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            //To Check, If Permission is granted
            getOneTimeLocation();
            subscribeLocationLocation();
          } else {
            console.log('Permission Denied');
          }
        } catch (err) {
          console.log("err      "+err);
        }
      }
    };
    requestLocationPermission();
    return () => {
      Geolocation.clearWatch(watchID);
    };
  }, []);
      

    const getOneTimeLocation = async () => {

      Geolocation.getCurrentPosition(
        //Will give you the current location
        (position) => {
  
          //getting the Longitude from the location json
          const currentLongitude = JSON.stringify(position.coords.longitude);
  
          //getting the Latitude from the location json
          const currentLatitude = JSON.stringify(position.coords.latitude);

          //Setting Longitude state
          setLongitude(position.coords.longitude);
          
          //Setting Longitude state
          setLatitude(position.coords.latitude);

          setMarker(position.coords)
          getAddressFromCoordinates(position.coords)

        },
        (error) => {
          openSettings()

          console.log("error.message getOneTimeLocation"+error.message);
        },
        {
          enableHighAccuracy: false,
          timeout: 30000,
          maximumAge: 1000
        },
      );
    };
    
    const subscribeLocationLocation = () => {
      watchID = Geolocation.watchPosition(
        (position) => {
          //Will give you the location on location change
          
          console.log("subscribeLocationLocation   "+position);
  
          //getting the Longitude from the location json        
          const currentLongitude = JSON.stringify(position.coords.longitude);
          //getting the Latitude from the location json
          const currentLatitude =  JSON.stringify(position.coords.latitude);
  
          //Setting Longitude state
          setLongitude(position.coords.longitude);
  
          //Setting Latitude state
          setLatitude(position.coords.latitude);

          setMarker(position.coords)
          getAddressFromCoordinates(position.coords)
        },
        (error) => {
          openSettings()
          console.log("error.message"+error.message);
        },
        {
          enableHighAccuracy: false,
          maximumAge: 1000
        },
      );
    };

// const pressPoint = (value ) => {
//   console.log("pressPoint"+value)
//   setMarker(value.nativeEvent.coordinate)
//   {
//     marker && getAddressFromCoordinates()
//   }
// }

const getAddressFromCoordinates = (position) => {
 console.log("H E R E")

  // Position Geocoding
  //if (marker) {
    var NY = {
      lat: position.latitude,
      lng: position.longitude
    }
  
  Geocoder.geocodePosition(NY).then(res => {
  
    console.log("geocodePosition : "+JSON.stringify(res))
    const {formattedAddress} = res[0]
    setAddress(formattedAddress)
    console.log(formattedAddress)
    
  })
  .catch(err => console.log(err))
 // }
  

// // Address Geocoding
// Geocoder.geocodeAddress('New York').then(res => {
//     // res is an Array of geocoding object (see below)
// })
// .catch(err => console.log(err))

}

  return (
    <ScrollView style ={styles.container} contentContainerStyle = {{alignItems:'center'}}>

        {/*................. FIRST VIEW HOLDING THE IMAGES................ */}

        {/* <CustomImagePicker /> */}

      <View style ={styles.mapView}>
      <MapView
       provider={PROVIDER_GOOGLE} // remove if not using Google Maps
       style={styles.map}
       region={{
         latitude: latitude,
         longitude: longitude,
         latitudeDelta: 0.015,
         longitudeDelta: 0.0121,
       }}

       //onPress = {(e) => pressPoint(e)}
     >
        {
          marker && 
          <MapView.Marker coordinate={marker} />
      }
     </MapView>
  </View>

  <View style = {styles.commentView}>
    <Text style={{color:'#fff',fontSize:12}}>{address}</Text>
  </View>

    {/* <View style ={styles.commentView}>
        
       <TextInput
          placeholder="Your Comments..."
          selectTextOnFocus={false}
          placeholderTextColor="#fff"
          color='#fff'
          fontSize={15}
        >
       </TextInput>
    </View> */}

    {/* <TouchableOpacity style ={{height:80}}
    onPress={()=>alert("Submitted")}
    >
      <Text style ={styles.button}>SUBMIT</Text>
    </TouchableOpacity> */}

    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor:'#d02860'
  },
  mapView:{
    backgroundColor:'green',
    //...StyleSheet.absoluteFillObject,
   height: 400,
   width: '90%',
   justifyContent: 'flex-end',
   alignItems: 'center',
   marginTop:15

  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  commentView:{
    backgroundColor:'rgba(0,0,0,0.2)' ,
      justifyContent:'center',
    alignItems:'center',
    width:'90%',
    marginTop:10,
    height:200,
    borderRadius:10  },
  button :{
    fontWeight:'bold',
    fontSize:40,
    color:'#fff'
  }
});

export default App;
